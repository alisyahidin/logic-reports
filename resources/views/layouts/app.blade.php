<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Logic</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/modules/pondokit-logic/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/modules/pondokit-logic/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/modules/pondokit-logic/bulma/css/bulma.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/modules/pondokit-logic/bulma/css/bulma-checkradio.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/modules/pondokit-logic/bulma/css/bulma-radio-checkbox.min.css') }}">
    <script src="/modules/pondokit-logic/js/app.js"></script>
    <script src="{{ asset('/modules/pondokit-logic/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/modules/pondokit-logic/js/vue/vue.min.js') }}"></script>
    <script src="{{ asset('/modules/pondokit-logic/js/axios/axios.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="/modules/pondokit-logic/select2/dist/css/select2.min.css">
    <script src="/modules/pondokit-logic/select2/dist/js/select2.min.js"></script>
</head>
<body>
    <main>
        @yield('content')
    </main>

    @if (session()->has('user_participant'))
        <div class="has-text-centered" style="margin-bottom: 13vh;">
            <form action="{{ route('logic.logout') }}" method="POST">
                {{ csrf_field() }}
                <button class="button is-danger is-outlined" type="submit">Logout</button>
            </form>
        </div>
    @endif

    {{-- Script --}}
    @stack('script')
</body>
</html>
