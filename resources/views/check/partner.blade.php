@extends('logic::check.base')

@section('check-content')
<div class="column is-12" id="partner_test">
    <div class="has-text-centered box">
        <h2 class="is-4 title is-danger is-fullwidth">
            {{ "You are checking $partner->name's logic" }}
        </h2>
    </div>
    <table class="table is-bordered is-fullwidth">
        <tr>
            <th></th>
            @for ($i = 1; $i <= $sub_question; $i++)
                <td style="text-align: center;" class="head_table">{{ $i }}</td>
            @endfor
        </tr>
        @for ($i = 0; $i < $question; $i++)
        <tr>
            <td>{{ $partner->getTestsToday()[$i]->number }}</td>
            @foreach($partner->getTestsToday()[$i]->results as $key => $result)
            <td>
                <div class="control">
                    <div class="field">
                        <input class="checked_false_{{$i+1}} is-checkradio is-danger" id="{{$i+1 . '_' . $key}}_false" type="radio" name="{{$i+1 . '_' . $key}}" {{ $result == "0" ? 'checked' : '' }} value="0">
                        <label for="{{$i+1 . '_' . $key}}_false"></label>

                        <input class="checked_true_{{$i+1}} is-checkradio is-info is-rtl" id="{{$i+1 . '_' . $key}}_true" type="radio" name="{{$i+1 . '_' . $key}}" {{ $result == "1" ? 'checked' : '' }} value="1">
                        <label for="{{$i+1 . '_' . $key}}_true"></label>
                    </div>
                </div>
            </td>
            @endforeach
            {{-- <td class="all-check">
                <input class="is-checkradio has-background-color is-success" id="{{ $i+1 }}" type="checkbox">
                <label for="{{ $i+1 }}"></label>
            </td> --}}
        </tr>
        @endfor
    </table>
</div>
@endsection