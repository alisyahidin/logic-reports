@extends('logic::layouts.app')

@section('content')
<div class="container">
    @if ( session('message') )
        <h3 class="notification is-3 is-danger is-fullwidth message-notif has-text-centered" style="margin-top: 3vh;">
            <button class="delete message-delete"></button>
            {{ session('message') }}
        </h3>
    @endif
</div>

<form method="POST">
{{ csrf_field() }}
<section class="hero" style="padding-bottom: 15vh;">
    <div class="container">
        @yield('check-content')
        <div class="column is-2 is-offset-5">
            <button type="submit" class="button is-medium is-fullwidth is-info">Save</button>
        </div>
    </div>
</section>
</form>

<script>
    $('document').ready(function () {
        $('#1').change(function() {
            if (this.checked) {
                $('.checked_false_1').removeAttr('checked')
                $('.checked_true_1').attr('checked', 'checked')
            } else {
                $('.checked_true_1').removeAttr('checked')
                $('.checked_false_1').attr('checked', 'checked')
            }
        });
        $('.message-delete').click(function () {
            $('.message-notif').hide();
        })
        $('#2').change(function() {
            if (this.checked) {
                $('.checked_false_2').removeAttr('checked')
                $('.checked_true_2').attr('checked', 'checked')
            } else {
                $('.checked_true_2').removeAttr('checked')
                $('.checked_false_2').attr('checked', 'checked')
            }
        });
        $('#3').change(function() {
            if (this.checked) {
                $('.checked_false_3').removeAttr('checked')
                $('.checked_true_3').attr('checked', 'checked')
            } else {
                $('.checked_true_3').removeAttr('checked')
                $('.checked_false_3').attr('checked', 'checked')
            }
        });
        $('#4').change(function() {
            if (this.checked) {
                $('.checked_false_4').removeAttr('checked')
                $('.checked_true_4').attr('checked', 'checked')
            } else {
                $('.checked_true_4').removeAttr('checked')
                $('.checked_false_4').attr('checked', 'checked')
            }
        });
        $('#5').change(function() {
            if (this.checked) {
                $('.checked_false_5').removeAttr('checked')
                $('.checked_true_5').attr('checked', 'checked')
            } else {
                $('.checked_true_5').removeAttr('checked')
                $('.checked_false_5').attr('checked', 'checked')
            }
        });
    });
</script>
@endsection
