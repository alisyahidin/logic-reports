@extends('logic::layouts.app')

@section('content')

<section class="hero">
    <div class="container">
        <div class="column is-12" id="table_results">

            <div class="box has-text-centered">
                <h3 class="title">Logic Results</h3>
                <h4>@{{ date }}</h4>
            </div>

            <table class="table is-bordered is-fullwidth">
                <tr class="head_table cursor-default">
                    <td style="text-align: center; width: 20%;">Name</td>
                    <td style="text-align: center; width: 20%;">Corrected by</td>
                    @for ($i = 1; $i <= $question; $i++)
                        <td style="text-align: center;">{{ $i }}</td>
                    @endfor
                    <td style="text-align: center;" @click="sortByTotal">Total</td>
                </tr>

                <td v-if="participants != null && participants.length == 0"
                    class="has-text-centered"
                    :colspan="participants.length == 0 ? 0 : 8"
                >
                    There is no data.
                </td>

                <tr v-for="participant in participants">
                    <td style="padding-left: 1vw;" :class="{ absen_user : isAbsen(participant), winner_user : isWinner(participant) }">
                        @{{ participant.name }}
                    </td>
                    <td style="padding-left: 1vw;" v-if="participant.tests != null" :class="{ alone_user : isAlone(participant) }">
                        @{{ participant.partner }}
                    </td>
                    </td>
                    <td style="text-align: center;" v-for="test in participant.tests" :class="{ warn_user : isZero(test), test_save : isSafe(test) }">
                        @{{ test }}
                    </td>
                    <td style="text-align: center;" v-if="participant.total_tests != null" :class="{ winner_user : isWinner(participant) }">
                        @{{ participant.total_tests }}
                </tr>
            </table>
            <nav class="navbar is-fixed-bottom is-fullwidth">
                    @if (! is_null($user))
                    <div class="navbar-start">
                        <a href="{{ $user->hasPartner() ? '/logic/check-partner' : '/logic/check' }}" class="button is-large margin-handap is-pulled-left is-success" style="margin-left: 1vw;">
                            <i class="fa fa-arrow-left"></i>
                        </a>
                    </div>
                    @endif
                <div class="navbar-end">
                    <button @click="updateParticipants" class="button is-large margin-handap is-info" style="right: 10px;">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </nav>
        </div>
    </div>
</section>

<script>
    var app = new Vue({
      el: '#table_results',
      data: {
        participants: null,
        sorted_participants: true,
        date: (new Date).toLocaleDateString()
      },
      methods: {
        getParticipants: function () {
            axios
                .get('/api/logic-participants')
                .then(response => (this.participants = response.data.data))
                .catch(error => alert(error));
        },
        updateParticipants: function () {
            this.getParticipants()
        },
        sortByTotal: function () {
            sorted = this.participants.sort((x, y) => x.total_tests - y.total_tests);
            if (this.sorted_participants) {
                this.participants = sorted.reverse();
                this.sorted_participants = false;
            } else {
                this.participants = sorted;
                this.sorted_participants = true;
            }
        },
        isWarning: function (participant) {
            return participant.total_tests == 0
                ? true
                : false;
        },
        isZero(test) {
            return test == 0
                ? true
                : false;
        },
        isSafe(test) {
            return test == 8
                ? true
                : false;
        },
        isAlone(participant) {
            return participant.partner == null && participant.tests != null
                ? true
                : false;
        },
        isWinner(participant) {
            return this.highestResults == participant.total_tests
                ? true
                : false;
        },
        isAbsen(participant) {
            return participant.tests == null
                ? true
                : false;
        }
      },
      mounted() {
        this.getParticipants()
      },
      computed: {
        highestResults: function () {
            total = [0];
            if (this.participants != null) {
                total = this.participants.map((user) => user.total_tests).filter(x => x);
            }

            return Math.max(...total);
        }
      }
    })
</script>

@endsection
