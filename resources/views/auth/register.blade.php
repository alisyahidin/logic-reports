@extends('logic::layouts.app')

@section('content')
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h1 class="subtitle has-text-white">Please input unique username</h1>
                    <form id="login" method="POST" action="{{ route('logic.register') }}">
                    {{ csrf_field() }}
                        <div class="box">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" name="name" placeholder="Unique username">
                                </div>
                            </div>
                        </div>

                        @if ($errors->any())
                            <div class="notification is-danger">
                            @foreach ($errors->all() as $error)
                                    <button type="button" class="delete"></button>
                                    <p>{{ $error }}</p>
                            @endforeach
                            </div>
                        @endif

                        <input class="button is-block is-info is-large is-fullwidth" type="submit" name="login" value="Register">
                    </form>
                </div>
                @if ( session('error') )
                    <h3 class="button is-3 is-danger">{{ session('error') }}</h3>
                @endif
            </div>
        </div>
    </section>

    <script>
        $('document').ready(function () {
            $('.delete').click(function () {
                $('.notification').hide()
            })
        })
    </script>
@endsection