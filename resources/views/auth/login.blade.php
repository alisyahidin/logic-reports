@extends('logic::layouts.app')

@section('content')
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">

                    @if ( session('message') )
                        <h3 class="notification is-3 is-danger message-notif">
                            <button type="button" class="delete message-delete"></button>
                            {{ session('message') }}
                        </h3>
                    @endif

                    <form id="login" method="POST" action="{{ $route }}">
                    {{ csrf_field() }}
                    <div id="form_search" class="box" style="background: transparent;">
                        <h1 class="subtitle has-text-white">Please login as</h1>
                        <div class="box">
                            <div class="field">
                                <div class="control">
                                    <select style="width: 100%;" class="select_participant" name="participant">
                                      <option v-for="result in data_results" :value="result.id">@{{ result.name }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if ($alone)
                        <h1 class="subtitle has-text-white">Select your partner</h1>
                        <div class="box" id="select_partner">
                            <div class="field">
                                <div class="control">
                                    <select style="width: 100%;" class="select_participant" name="partner">
                                      <option v-for="result in data_results" :value="result.id">@{{ result.name }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                        </div>

                        @if ( session('error') )
                            <h3 class="notification is-3 is-danger error-notif">
                                <button type="button" class="delete error-delete"></button>
                                {{ session('error') }}
                            </h3>
                        @endif

                        <input class="button is-block is-info is-large is-fullwidth" type="submit" name="login" value="Login">
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script>
        var form = new Vue({
            el: '#form_search',
            data: {
                search_query: '',
                data_results: []
            },
            methods: {
                getParticipants() {
                    axios.get('/api/logic-participants')
                    .then(response => {
                        this.data_results = response.data.data;
                    })
                }
            },
            mounted() {
                this.getParticipants();
            }
        })

        function formatState (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $(
                '<b>' + state.text + '</b>'
            );

            return $state;
        };

        $('document').ready(function () {
            $('.message-delete').click(function () {
                $('.message-notif').hide();
            })

            $('.error-delete').click(function () {
                $('.error-notif').hide();
            })

            $('.select_participant').select2({
                // templateResult: formatState
            });
        });
    </script>

@endsection
