<?php /*** Bismillahirrahmanirrahim ***/

namespace PondokIT\Logic;

use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Publish asset
        $this->publishes([
            __DIR__.'/../assets' => public_path('modules/pondokit-logic'),
        ], 'public');
    }

    public function register()
    {
        //
    }

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}