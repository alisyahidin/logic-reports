<?php

namespace PondokIT\Logic\Http\Middleware;

use Closure;
use PondokIT\Logic\Models\Participant;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class SignedInMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $client = null)
    {
        if ($request->session()->has('user_participant')) {
            $participant = Participant::find(session('user_participant'));
            return $participant->hasPartner()
                ? redirect()->route('logic.checkPartner')
                : redirect()->route('logic.check');
        }

        return $next($request);
    }
}
