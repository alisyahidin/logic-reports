<?php

namespace PondokIT\Logic\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthLogicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $client = null)
    {
        if (! $request->session()->has('user_participant')) {
            return redirect()->route('logic.login');
        }

        return $next($request);
    }
}
