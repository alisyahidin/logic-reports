<?php

namespace PondokIT\Logic\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Test;
use PondokIT\Logic\Models\Participant;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkPartner()
    {
        $participant = Participant::find(session()->get('user_participant'));

        return view('logic::check.partner', [
            'team' => $participant->getTeam(),
            'partner' => $participant->getPartner(),
            'question' => Test::QUESTION_NUMBER,
            'sub_question' => Test::SUB_QUESTION_NUMBER
        ]);
    }

    public function check()
    {
        $participant = Participant::find(session()->get('user_participant'));

        return view('logic::check.self', [
            'sub_question' => Test::SUB_QUESTION_NUMBER,
            'question' => Test::QUESTION_NUMBER,
            'user' => $participant
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $participant = Participant::find(session('user_participant'));

        foreach ($request->except('_token') as $key => $value) {
            $number = explode('_', $key);
            $results[$number[0]][$number[1]] = $value;
        }

        foreach ($results as $number => $result) {
            $this->updateTest($number, $result, $participant->getPartner()->id);
        }

        return redirect()->route('logic.results');
    }

    public function saveAlone(Request $request)
    {
        $participant = Participant::find(session('user_participant'));

        foreach ($request->except('_token') as $key => $value) {
            $number = explode('_', $key);
            $results[$number[0]][$number[1]] = $value;
        }

        foreach ($results as $number => $result) {
            $this->updateTest($number, $result, $participant->id);
        }

        return redirect()->route('logic.results');
    }

    public function updateTest(int $number, array $results, $id)
    {
        $test = Test::where('number', $number)
                ->where('participant_id', $id)
                ->where('date', date('y-m-d'))
                ->first();

        $test->results = $results;
        $test->save();
    }

    public function results()
    {
        return view('logic::results', [
            'user' => Participant::find(session('user_participant')),
            'participants' => Participant::all(),
            'question' => Test::QUESTION_NUMBER
        ]);
    }
}
