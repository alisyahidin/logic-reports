<?php

namespace PondokIT\Logic\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Participant;
use PondokIT\Logic\Models\Team;
use PondokIT\Logic\Models\Test;

trait LoginWithPartner
{
    public function loginWithPartner(Request $request)
    {
        $participant = Participant::find($request->participant);

        if ($this->isDuplicateParticipant($request)) {
            return back()->with('error', 'User and partner cannot the same people');
        }

        if (! $participant->hasTeam() && $request->partner == 0) {
            return back()->with('error', 'You must select a partner or you can login alone :)');
        }

        if (! $participant->hasTeam() && ! is_null($participant->getTestsToday())) {
            return back()->with('error', 'You must login alone');
        }

        if (! $this->checkTeam($request->participant, $request->partner)) {
            $partner = Participant::find($request->partner);
            return back()->with('error', 'You cannot pick ' . $partner->name . ' as a partner');
        }

        $this->checkTest($participant->getTeam());
        session(['user_participant' => $request->participant]);

        return redirect()->route('logic.checkPartner');
    }

    public function checkTest(Team $team)
    {
        $tests = $team->participants->map(function ($participant) {
            return $participant->getTestsToday();
        })->toArray();

        if (! count(array_filter($tests))) {
            $this->loadTest($team);
        }
    }

    public function loadTest(Team $team)
    {
        foreach ($team->participants as $participant) {
            $this->createTest(Test::QUESTION_NUMBER, Test::SUB_QUESTION_NUMBER, $participant);
        }
    }

    public function createTest($question_number, $sub_question_number, Participant $participant)
    {
        for ($i=1; $i <= $question_number; $i++) {
            for ($x=1; $x <= $sub_question_number; $x++) {
                $results[$x] = 0;
            }
            $test = new Test();
            $test->number = $i;
            $test->results = $results;
            $test->participant()->associate($participant);
            $test->save();
        }
    }

    public function checkTeam(int $participant_id, int $partner_id)
    {
        $participant = Participant::find($participant_id);
        $partner = Participant::find($partner_id);

        if ($this->hasTeam($participant)) {
            $this->loginWithExistingPartner($participant, $partner_id);
        }

        if (! $this->hasTeam($participant) && ! $this->hasTeam($partner)) {
            $this->createTeam($participant_id, $partner_id);
        }

        if (! $this->hasTeam($participant) && $this->hasTeam($partner)) {
            return false;
        }

        return true;
    }

    public function loginWithExistingPartner(Participant $participant, int $partner_id)
    {
        $member = $participant->getTeam()->participants->map(function ($participant) {
            return $participant->id;
        })->toArray();

        if (! in_array($partner_id, $member)) {
            session()->flash('message','you have been partenered with ' . $participant->getPartner()->name);
        }
    }

    public function createTeam($participant_id, $partner_id)
    {
        $participant = Participant::find($participant_id);
        $partner = Participant::find($partner_id);

        $team = new Team();
        $team->save();
        $team->participants()->save($participant);
        $team->participants()->save($partner);

        return $team;
    }

    public function hasTeam(Participant $user)
    {
        return $user->teams()->today()->count();
    }

    public function isDuplicateParticipant(Request $request)
    {
        return $request->participant == $request->partner;
    }
}