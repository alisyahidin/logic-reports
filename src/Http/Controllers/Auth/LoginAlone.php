<?php

namespace PondokIT\Logic\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Participant;
use PondokIT\Logic\Models\Team;
use PondokIT\Logic\Models\Test;

trait LoginAlone
{
    public function loginAlone(Request $request)
    {
        $participant = Participant::find($request->participant);

        if ($participant->hasTeam()) {
            return redirect()->route('logic.login')->with('error', 'You cannot login alone');
        }

        $this->checkTestAlone($participant);
        session(['user_participant' => $request->participant]);

        return redirect()->route('logic.check');
    }

    public function checkTestAlone(Participant $participant)
    {
        $participant->tests()->today()->get()->count() ?: $this->createTestAlone($participant);
    }

    public function createTestAlone(Participant $participant)
    {
        for ($i=1; $i <= Test::QUESTION_NUMBER; $i++) {
            for ($x=1; $x <= Test::SUB_QUESTION_NUMBER; $x++) {
                $results[$x] = 0;
            }
            $test = new Test();
            $test->number = $i;
            $test->results = $results;
            $test->participant()->associate($participant);
            $test->save();
        }
    }
}