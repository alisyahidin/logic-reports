<?php

namespace PondokIT\Logic\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Participant;

class RegisterController extends Controller
{
    use LoginWithPartner, LoginAlone;

    public function view()
    {
        return view('logic::auth.register');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => [
                'bail', 'required', 'unique:logic_participants,name', 'max:10',
                'regex:/(^([a-zA-z]+)(\d+)?$)/u'
            ]
        ]);

        $participant = Participant::create($data);

        return redirect()->route('logic.login')
            ->with('message', "user {$participant->name} created.");
    }
}
