<?php

namespace PondokIT\Logic\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Participant;
use PondokIT\Logic\Models\Team;
use PondokIT\Logic\Models\Test;

class LoginController extends Controller
{
    use LoginWithPartner, LoginAlone;

    public function view()
    {
        $route = route('logic.loginWithPartner');
        $alone = false;

        if (request()->has('type') && request()->input('type') == 'alone') {
            $route = route('logic.loginAlone');
            $alone = true;
        }

        return view('logic::auth.login', [
            'route' => $route,
            'alone' => !$alone
        ]);
    }

    public function logout(Request $request)
    {
        $request->session()->forget('user_participant');

        return redirect()->route('logic.login');
    }
}
