<?php

namespace PondokIT\Logic\Http\ApiControllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PondokIT\Logic\Models\Participant;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participant = Participant::all();

        if (request()->has('search')) {
            $participant = Participant::where('name', 'like', '%'.request()->input('search').'%')->get();
        }

        $data = $participant->map(function ($participant) {
            return [
                'id' => $participant->id,
                'name' => $participant->name,
                'tests' => is_null($participant->getTestsToday()) ? null :
                    $participant->getTestsToday()->map(function ($test) {
                        return $test->average;
                    }),
                'total_tests' => $participant->total_test_result,
                'partner' => $participant->hasPartner() ? $participant->getPartner()->name : null
            ];
        });

        return $this->transform($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $participant
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $logicParticipant)
    {
        $data = Participant::find($logicParticipant);

        return $this->transform($data);
    }

    public function showTests(Request $request, $logicParticipant)
    {
        $data = Participant::find($logicParticipant)->tests;

        if ($request->has('date') && $request->input('date') == 'today') {
            $data = Participant::find($logicParticipant)->getTestsToday();
        }

        return $this->transform($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transform($data)
    {
        return [
            'data' => $data
        ];
    }
}
