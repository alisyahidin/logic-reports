<?php /*** Bismillahirrahmanirrahim ***/

namespace PondokIT\Logic\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Test extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logic_tests';

    protected $fillable = [
        'number', 'results'
    ];

    protected $casts = [
        'results' => 'array',
    ];

    public const QUESTION_NUMBER = 5;

    public const SUB_QUESTION_NUMBER = 8;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->date = date('y-m-d');
        });
    }

    public function participant()
    {
        return $this->belongsTo(Participant::class);
    }

    public function scopeToday($query)
    {
        return $query->where('date', date('y-m-d'));
    }

    public function getAverageAttribute()
    {
        return array_sum(array_values($this->results));
    }
}
