<?php /*** Bismillahirrahmanirrahim ***/

namespace PondokIT\Logic\Models;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logic_participants';

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'logic_participant_team');
    }

    public function tests()
    {
        return $this->hasMany(Test::class);
    }

    public function getPartner()
    {
        return is_null($this->getTeam()) ? null : $this->getTeam()->participants()->where('id', '!=', $this->id)->first();
    }

    public function hasPartner()
    {
        return $this->getTeam();
    }

    public function hasTeam()
    {
        return ! is_null($this->getTeam());
    }

    public function getTeam()
    {
        return $this->teams()->today()->count() ?
            $this->teams()->today()->first() : null;
    }

    public function getTestsToday()
    {
        return $this->tests()->today()->get()->count() ?
            $this->tests()->today()->get() : null;
    }

    public function getTotalTestResultAttribute()
    {
        $tests = is_null($this->getTestsToday()) ? null
            : $this->getTestsToday()->map(function ($test) {
                return $test->average;
            })->toArray();

        return is_null($tests) ? null : array_sum(array_values($tests));
    }
}
