<?php /*** Bismillahirrahmanirrahim ***/

namespace PondokIT\Logic\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Team extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logic_teams';

    protected $fillable = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->date = Carbon::today();
        });
    }

    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'logic_participant_team');
    }

    public function scopeToday($query)
    {
        return $query->where('date', date('y-m-d'));
    }
}
