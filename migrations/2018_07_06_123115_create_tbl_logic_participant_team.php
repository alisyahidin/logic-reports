<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLogicParticipantTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logic_participant_team', function (Blueprint $table) {
            $table->unsignedInteger('team_id');
            $table->unsignedInteger('participant_id');
            $table->foreign('participant_id')->references('id')->on('logic_participants');
            $table->foreign('team_id')->references('id')->on('logic_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logic_participant_team');
    }
}
