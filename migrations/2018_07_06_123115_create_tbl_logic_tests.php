<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLogictests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logic_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->tinyInteger('number');
            $table->json('results')->nullable();
            $table->unsignedInteger('participant_id');
            $table->foreign('participant_id')->references('id')->on('logic_participants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logic_tests');
    }
}
