<?php

Route::apiResource('logic-participants', ParticipantController::class);

Route::prefix('logic-participants')->group(function () {
    Route::get('/{participant}/tests', ParticipantController::class.'@showTests');
});
