<?php

use PondokIT\Logic\Http\Middleware\AuthLogicMiddleware;
use PondokIT\Logic\Http\Middleware\SignedInMiddleware;

Route::get('/', function () {
    return redirect()->route('logic.login');
});

Route::prefix('register')->group(function () {
    Route::get('/', Auth\RegisterController::class.'@view');
    Route::post('/', Auth\RegisterController::class.'@store')
        ->name('logic.register');
});

Route::prefix('login')->group(function() {
    Route::get('/', Auth\LoginController::class.'@view')
        ->middleware(SignedInMiddleware::class)
        ->name('logic.login');
    Route::post('/with-partner', Auth\LoginController::class.'@loginWithPartner')
        ->name('logic.loginWithPartner');
    Route::post('/alone', Auth\LoginController::class.'@loginAlone')
        ->name('logic.loginAlone');
});

Route::post('logout', Auth\LoginController::class.'@logout')
    ->name('logic.logout');

Route::prefix('check-partner')->group(function() {
    Route::get('/', TestController::class.'@checkPartner')
        ->name('logic.checkPartner')
        ->middleware(AuthLogicMiddleware::class);
    Route::post('/', TestController::class.'@save');
});

Route::prefix('check')->group(function() {
    Route::get('/', TestController::class.'@check')
        ->name('logic.check')
        ->middleware(AuthLogicMiddleware::class);
    Route::post('/', TestController::class.'@saveAlone');
});

Route::get('results', TestController::class.'@results')
    ->name('logic.results');