## Installation

Add repository to composer.json in your laravel project.

```sh
"repositories": [
    {
        "url": "https://gitlab.com/alisyahidin/logic-reports.git",
        "type": "git"
    }
]
```

And in your require, add this package `"pondokit/logic": "dev-master"`

```sh
"require": {
    ...
    "pondokit/logic": "dev-master"
    ...
}
```

Update your composer

```sh
composer update
```

## Post Installation

run this command :

`php artisan migrate`

`php artisan vendor:publish --tag=public`